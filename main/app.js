function distance(first, second){
	// verificare tipul parametrilor
	if (!Array.isArray(first) || !Array.isArray(second))
		throw new Error('InvalidType');

	// eliminare duplicate din array
	// Creand un obiect de tip Set dintr-un array
	// se vor elimina automat duplicatele, data fiind
	// implementarea acestui obiect.
	// Creez din nou un array din acel set deoarece
	// vreau sa folosesc metoda 'find'
	let firstF = Array.from(new Set(first));
	let secondF = Array.from(new Set(second));

	// functie pentru a determina
	// 'distanta partiala', adica distanta prin care
	// verific un array fata de altul, dar nu si invers
	let partialDistance = (first, second) => {
		let dist = 0; // contor pentru a masura distanta
		for (let eF of first) {
			let found = second.find(function(eS) {
				return eS === eF;
			});
			// daca elementul x din primul array nu a fost
			// gasit in al doilea array, contorul se va incrementa
			if (typeof found == 'undefined') {
				dist++;
			}
		}
		return dist;
	}

	return partialDistance(firstF, secondF) + partialDistance(secondF, firstF);
}

module.exports.distance = distance